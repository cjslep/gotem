package configured

import (
	"gopkg.in/v1/yaml"
	"io/ioutil"
)

const (
	FILE_SWITCH_ABSTRACT_FORMS = "switchAbstractForm.tmpl"
)

type SwitchInputs struct {
	InputId string // Required
	HasLabel bool
	Label string // Only if HasLabel
	Type string // Only if !IsTextArea
	Name string
	Value string
	CssClass string
	IsTextArea bool
	Cols int // Only if IsTextArea
	Rows int // Only if IsTextArea
	Required bool // Only if IsTextArea
	Spellcheck bool // Only if IsTextArea
}

type SwitchAbstractForm struct {
	SubmitUri string
	Method string
	FormId string
	SwitchInputs []*SwitchInputs
}

func LoadSwitchAbstractFormFromYAML(filename string) (*SwitchAbstractForm, error) {
	cont, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	c := SwitchAbstractForm{}
	err = yaml.Unmarshal(cont, &c)
	if err != nil {
		return nil, err
	}
	return &c, nil
}