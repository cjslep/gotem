package configured

import (
	"gopkg.in/v1/yaml"
	"io/ioutil"
	"crypto/rand"
)

const (
	FILE_ABSTRACT_FORMS = "abstractForm.tmpl"
)

type RandomNameInput struct {
	MakeRandom bool
	SessionSave string
}

type LabeledInputs struct {
	Label string
	Type string
	Name string
	Value string
	CssClass string
	MakeNameRandom *RandomNameInput
}

type Inputs struct {
	Type string
	Name string
	Value string
	CssClass string
	MakeNameRandom *RandomNameInput
}

type AbstractForm struct {
	SubmitUri string
	Method string
	LabeledInputs []*LabeledInputs
	Inputs []*Inputs
	RandStringLength int
}

func LoadAbstractFormFromYAML(filename string) (*AbstractForm, error) {
	cont, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	c := AbstractForm{}
	err = yaml.Unmarshal(cont, &c)
	if err != nil {
		return nil, err
	}
	c.RandomizeNames()
	return &c, nil
}

func (a *AbstractForm) RandomizeNames() {
	for _, input := range a.Inputs {
		if input.MakeNameRandom != nil && input.MakeNameRandom.MakeRandom {
			input.Name = randString(a.RandStringLength)
		}
	}
	for _, input := range a.LabeledInputs {
		if input.MakeNameRandom != nil && input.MakeNameRandom.MakeRandom {
			input.Name = randString(a.RandStringLength)
		}
	}
}

func randString(n int) string {
	const alphanum = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	var bytes = make([]byte, n)
	rand.Read(bytes)
	for i, b := range bytes {
		bytes[i] = alphanum[b%byte(len(alphanum))]
	}
	return string(bytes)
}