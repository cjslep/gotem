package goTem

import (
	"html/template"
	"io"
	"io/ioutil"
	"code.google.com/p/go.exp/fsnotify"
	"strings"
	"path/filepath"
	"errors"
)

type Logger interface {
	Println(string)
}

type executeRequest struct {
	Writer io.Writer
	Name string
	Data interface{}
	Return chan error
}

// Holds a list of template names to the files needed to successfully
// parse the template
type HTMLTemplateWatcher struct {
	running bool
	filePath string
	fileExtension string
	logger Logger
	templateWatcher   *fsnotify.Watcher
	templates *template.Template
	stop      chan bool
	execute   chan executeRequest
}

func getFileNamesInDirWithExtension(path string, extension string) ([]string, error) {
	fileInfos, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, err
	}
	var results []string = nil
	for _, fileInfo := range fileInfos {
		if !fileInfo.IsDir() && strings.HasSuffix(fileInfo.Name(), extension) {
			results = append(results, filepath.Join(path, fileInfo.Name()))
		}
	}
	return results, nil
}

func NewHTMLTemplateWatcher(filePath, fileExtension string, logger Logger) (*HTMLTemplateWatcher, error) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return nil, err
	}

	files, err := getFileNamesInDirWithExtension(filePath, fileExtension)
	if err != nil {
		return nil, err
	}

	templates, err := template.ParseFiles(files...)
	if err != nil {
		return nil, err
	}

	temp := HTMLTemplateWatcher{false, filePath, fileExtension, logger, watcher, templates, make(chan bool, 1), make(chan executeRequest, 1)}
	return &temp, nil
}

func (h *HTMLTemplateWatcher) Start() {
	if h.running {
		return
	}
	h.running = true
	h.logger.Println("HTMLTemplateWatcher watching: " + h.filePath + "*" + h.fileExtension)
	h.templateWatcher.Watch(h.filePath)
	go func() {
		for {
			select {
			case _ = <-h.stop:
				return

			case event := <-h.templateWatcher.Event:
				if strings.HasSuffix(event.Name, h.fileExtension) {
					h.logger.Println("HTMLTemplateWatcher FSNotify: " + event.String())
					
					/*
					This would be the smarter way of doing it, but for now there is not an
					obvious way to remove a template associate with h.templates by name, so
					re-parsing everything is the temporary / dumber solution
					
					if event.IsModify() || event.IsRename() || event.IsCreate() {
						temp, err := h.templates.ParseFiles(event.Name)
						if err != nil {
							h.logger.Println("HTMLTemplateWatcher FSNotify ParseFiles error: " + err.Error())
						} else {
							h.logger.Println("HTMLTemplateWatcher FSNotify: Added file \"" + event.Name + "\"")
							h.templates = temp
						}
					} else if event.IsDelete() {
						files, err := getFileNamesInDirWithExtension(h.filePath, h.fileExtension)
						if err != nil {
							h.logger.Println("HTMLTemplateWatcher FSNotify getFileNamesInDirWithExtension error: " + err.Error())
						} else {
							templ, err := template.ParseFiles(files...)
							if err != nil {
								h.logger.Println("HTMLTemplateWatcher FSNotify ParseFiles error: " + err.Error())
							} else {
								h.templates = templ
							}
						}
					}
					*/

					if event.IsModify() || event.IsRename() || event.IsCreate() || event.IsDelete() {
						files, err := getFileNamesInDirWithExtension(h.filePath, h.fileExtension)
						if err != nil {
							h.logger.Println("HTMLTemplateWatcher FSNotify getFileNamesInDirWithExtension error: " + err.Error())
						} else {
							templ, err := template.ParseFiles(files...)
							if err != nil {
								h.logger.Println("HTMLTemplateWatcher FSNotify ParseFiles error: " + err.Error())
							} else {
								h.templates = templ
							}
						}
					}

				}

			case err := <-h.templateWatcher.Error:
				h.logger.Println("HTMLTemplateWatcher FSNotify error: " + err.Error())

			case execute := <-h.execute:
				execute.Return <- h.templates.ExecuteTemplate(execute.Writer, execute.Name, execute.Data)

			}
		}
		h.logger.Println("HTMLTemplateWatcher stopping watching: " + h.filePath + "*" + h.fileExtension)
		h.templateWatcher.Close()
		h.running = false
	}()
}

func (h *HTMLTemplateWatcher) Stop() {
	if h.running {
		h.stop <- true
	}
}

func (h *HTMLTemplateWatcher) ExecuteTemplate(wr io.Writer, templateName string, data interface{}) error {
	if h.running {
		retChan := make(chan error, 1)
		h.execute <-executeRequest{wr, templateName, data, retChan}
		return <-retChan
	} else {
		return errors.New("HTMLTemplateWatcher ExecuteTemplate error: Watcher has not started")
	}
}
