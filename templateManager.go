package goTem

import (
	"errors"
	"text/template"
)

// Holds a list of template names to the files needed to successfully
// parse the template
type templateManager struct {
	templateToFiles map[string]([]string)
}

func newTemplateManager() *templateManager {
	temp := templateManager{make(map[string]([]string))}
	return &temp
}

// Adds a filename and the associated dependent files needed when parsing the template
func (t *templateManager) AddTemplate(mainFile string, dependentTemplates []string) (string, error) {
	// Test to ensure able to parse the file with the dependent templates
	dependentTemplates = append(dependentTemplates, "")
	copy(dependentTemplates[1:], dependentTemplates[0:])
	dependentTemplates[0] = mainFile
	temp, err := template.ParseFiles(dependentTemplates...)
	if err != nil {
		return "", err
	}
	if _, ok := t.templateToFiles[mainFile]; ok {
		return "", errors.New("Template with name=\"" + mainFile + "\" already exists")
	}
	t.templateToFiles[mainFile] = dependentTemplates
	return temp.Name(), nil
}

// Returns the filenames needed to be parsed for the associated template
func (t *templateManager) GetFilenames(mainFile string) ([]string, error) {
	files, ok := t.templateToFiles[mainFile]
	if !ok {
		return nil, errors.New("Template with name=\"" + mainFile + "\" does not exist")
	}
	return files, nil
}
