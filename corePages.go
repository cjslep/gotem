package goTem

import (
	"bitbucket.org/cjslep/goTem/skeleton"
	"bitbucket.org/cjslep/goTem/constructs"
)

type CoreData struct {
	WebHUD     *constructs.WebHUD
	FooterData *skeleton.Footer
}

func NewCoreData(title string, stylesheets []string, forceJS bool, jScripts []string, sessionMessage string, messageIDCSS []string, menus []*skeleton.Menu) *CoreData {
	tHeaderData, tFooterData := skeleton.NewHeaderAndFooter(title, stylesheets, forceJS, jScripts)
	tDoubleNavData := constructs.NewMultiMenu(menus)
	var tSessionMessageData *constructs.MessageDiv
	if sessionMessage == "" {
		tSessionMessageData = constructs.NewNoMessageDiv(messageIDCSS)
	} else {
		tSessionMessageData = constructs.NewMessageDiv(messageIDCSS, sessionMessage)
	}
	tempCoreData := CoreData{constructs.NewWebHUD(tHeaderData, tDoubleNavData, tSessionMessageData), tFooterData}
	return &tempCoreData
}

func CoreDataFileDependencies() []string {
	return []string{constructs.FILE_COMPOSITES, skeleton.FILE_CONTENT_ELEMENTS, skeleton.FILE_HTML_ELEMENTS}
}
