{{/*
	Template "Header":
	.
	|- PageTitle string (eg: "My Site")
	|- JSMustEnable bool (True will display "This site requires Javascript to be enabled." if scripting is disabled)
	
	IDs:
		None
	Classes:
		pagecontainer (if JSMustEnable is true)
		noscriptmsg (if JSMustEnable is true && JS is disabled)
		
	Dependencies:
		Footer (contentElements.tmpl)
*/}}
{{define "Header"}}<!DOCTYPE html>
<html>
	<head>
		{{if .JSMustEnable}}<noscript>
			<style type="text/css">
				.pagecontainer {display:none;}
			</style>
		</noscript>{{end}}
		<title>{{.PageTitle}}</title>
	</head>
	<body>
		{{if .JSMustEnable}}<noscript>
			<div class="noscriptmsg">
				<p>This site requires Javascript to be enabled.</p>
			</div>
		</noscript><div class="pagecontainer">{{end}}{{end}}

{{/*
	Template "Footer":
	.
	|- JSMustEnable bool (must match "Header" value)
	|- []JSScripts []string
		|- scriptName (eg: "javascript.js")
	|- []Stylesheets []string
		|- stylesheetName (eg: "templates/stylesheets/test.css")
	
	IDs:
		None
	Classes:
		None
		
	Dependencies:
		Header (contentElements.tmpl)
*/}}
{{define "Footer"}}{{if .JSMustEnable}}</div>{{end}}
{{range .Stylesheets}}<link href={{.}} media="all" rel="stylesheet" type="text/css" />{{end}}
{{range .JSScripts}}<script src={{.}}></script>{{end}}</body>
</html>{{end}}

{{/*
	Template "Menu":
	.
	|- MenuIDCSS string (eg "NavBar" or "AdminNavBar")
	|- MenuChildren []menuChildData
		|- MenuChildData
		|- ...
		|- MenuChildData
			|- ChildClassCSS string (eg "NavContainer OnPage")
			|- Child MenuChild
				
	IDs:
		{{MenuIDCSS}}
	Classes:
		{{ChildClassCSS}}
		
	Dependencies:
		MenuChild (contentElements.tmpl)
*/}}
{{define "Menu"}}<div id={{.MenuIDCSS}}>{{range .MenuChildren}}<div class={{.ChildClassCSS}}>{{template "MenuChild" .Child}}</div>{{end}}</div>{{end}}

{{/*
	Template "MenuChild"
	.
	|- MenuTopicLink (LinkClass)
	|- MenuTopicString string
	|- []MenuChildLink [](LinkClass)
		|- MenuChildLink (LinkClass)
		|- ...
		|- MenuChildLink (LinkClass)

	IDs:
		None
	Classes:
		None
		
	Dependencies:
		LinkClass (htmlElements.tmpl)
*/}}
{{define "MenuChild"}}{{if .MenuTopicLink}}{{template "LinkClass" .MenuTopicLink}}{{else}}{{.MenuTopicString}}{{end}}<div>{{range .MenuChildLink}}{{template "LinkClass" .}}{{end}}</div>{{end}}

{{/*
	Template "NextPrevControls"
	.
	|- PrevClassCSS string
	|- PrevLink (LinkClass)
	|- NextClassCSS string
	|- NextLink (LinkClass)
	
	IDs:
		None
	Classes:
		{{PrevClassCSS}}
		{{NextClassCSS}}
	
	Dependencies:
		LinkClass (htmlElements.tmpl)
*/}}
{{define "NextPrevControls"}}<div class={{.PrevClassCSS}}>{{template "LinkClass" .PrevLink}}</div><div class={{.NextClassCSS}}>{{template "LinkClass" .NextLink}}</div>{{end}}