package skeleton

const (
	NAME_TEMPLATE_HEADER           = "Header"
	NAME_TEMPLATE_FOOTER           = "Footer"
	NAME_TEMPLATE_MENU             = "Menu"
	NAME_TEMPLATE_MENUCHILD        = "MenuChild"
	NAME_TEMPLATE_NEXTPREVCONTROLS = "NextPrevControls"

	FILE_CONTENT_ELEMENTS = "contentElements.tmpl"
)

// Data for Template "Header"
type Header struct {
	PageTitle    string
	JSMustEnable bool
}

// Data for Template "Footer"
type Footer struct {
	JSMustEnable bool
	JSScripts    []string
	Stylesheets  []string
}

func NewHeaderAndFooter(title string, stylesheets []string, forceJS bool, jScripts []string) (h *Header, f *Footer) {
	temp := Header{title, forceJS}
	tempFoot := Footer{forceJS, jScripts, stylesheets}
	return &temp, &tempFoot
}

// Data for Template "Menu"
type Menu struct {
	MenuIDCSS    string
	MenuChildren []*menuChildData
}

type menuChildData struct {
	ChildClassCSS string
	AllSessionVars []string
	NoSessionVars []string
	Child         *MenuChild
}

func NewMenu(idCSS string) *Menu {
	temp := Menu{idCSS, nil}
	return &temp
}

func (m *Menu) Clone() *Menu {
	toReturn := NewMenu(m.MenuIDCSS)
	for _, child := range m.MenuChildren {
		toReturn.AddChildWithSessionVars(child.ChildClassCSS, child.Child.Clone(), child.AllSessionVars, child.NoSessionVars)
	}
	return toReturn
}

func (t *Menu) AddChild(classCSS string, child *MenuChild) {
	temp := menuChildData{classCSS, nil, nil, child}
	t.MenuChildren = append(t.MenuChildren, &temp)
}

func (t *Menu) AddChildWithSessionVars(classCSS string, child *MenuChild, allSessVars, noSessVars []string) {
	temp := menuChildData{classCSS, allSessVars, noSessVars, child}
	t.MenuChildren = append(t.MenuChildren, &temp)
}

// Data for Template "MenuChild"
type MenuChild struct {
	MenuTopicLink   *LinkClass
	MenuTopicString string
	MenuChildLink   []*LinkClass
}

func NewMenuChildLinkedTopic(topicLink *LinkClass) *MenuChild {
	temp := MenuChild{topicLink, "", nil}
	return &temp
}

func NewMenuChildStringTopic(topic string) *MenuChild {
	temp := MenuChild{nil, topic, nil}
	return &temp
}

func (m *MenuChild) Clone() *MenuChild {
	var toReturn *MenuChild = nil
	if m.MenuTopicLink != nil {
		toReturn = NewMenuChildLinkedTopic(m.MenuTopicLink.Clone())
	} else {
		toReturn = NewMenuChildStringTopic(m.MenuTopicString)
	}
	for _, childLink := range m.MenuChildLink {
		toReturn.AddChild(childLink.Clone())
	}
	return toReturn
}

func (t *MenuChild) AddChild(child *LinkClass) {
	t.MenuChildLink = append(t.MenuChildLink, child)
}

// Data for Template "NextPrevControls"
type NextPrevControls struct {
	PrevClassCSS string
	PrevLink     *LinkClass
	NextClassCSS string
	NextLink     *LinkClass
}

func NewNextPrevControls(prevClassCSS, nextClassCSS string, prevLink, nextLink *LinkClass) *NextPrevControls {
	temp := NextPrevControls{prevClassCSS, prevLink, nextClassCSS, nextLink}
	return &temp
}
