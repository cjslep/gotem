package skeleton

import (
	"strings"
)

const (
	NAME_TEMPLATE_LINK      = "Link"
	NAME_TEMPLATE_LINKCLASS = "LinkClass"
	NAME_TEMPLATE_TABLE     = "Table"

	FILE_HTML_ELEMENTS = "htmlElements.tmpl"
)

// Data for Template "Link"
type Link struct {
	LinkPath        string
	LinkDisplayName string
}

func NewLink(path, displayName string) *Link {
	temp := Link{path, displayName}
	return &temp
}

// Data for template "LinkClass"
type LinkClass struct {
	LinkPath        string
	LinkDisplayName string
	Classes         string
}

func (l *LinkClass) Clone() *LinkClass {
	return NewLinkClass(l.LinkPath, l.LinkDisplayName, []string{l.Classes})
}

func NewLinkClass(path, displayName string, classes []string) *LinkClass {
	temp := LinkClass{path, displayName, strings.Join(classes, " ")}
	return &temp
}

// Data for template "Table"
type Table struct {
	Rows    []row
	Headers row
}

type row struct {
	Cells []string
}

func NewTable(headers []string) *Table {
	temp := Table{nil, row{headers}}
	return &temp
}

func (t *Table) AddRow(aRow []string) {
	t.Rows = append(t.Rows, row{aRow})
}

// Data for template "EditableDiv"
type EditableDiv struct {
	CSSID    string
	Editable bool
	Default  string
}

func NewEditableDiv(cssID string, editable bool, defaultText string) *EditableDiv {
	temp := EditableDiv{cssID, editable, defaultText}
	return &temp
}
