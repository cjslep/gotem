package constructs

import (
	"bitbucket.org/cjslep/goTem/skeleton"
	"strings"
)

const (
	NAME_TEMPLATE_MULTIMENU  = "MultiMenu"
	NAME_TEMPLATE_MESSAGEDIV = "MessageDiv"
	NAME_TEMPLATE_WEBHUD     = "WebHUD"

	FILE_COMPOSITES = "composites.tmpl"
)

// Data for Template "MultiMenu"
type MultiMenu struct {
	Menus []*skeleton.Menu
}

func NewMultiMenu(menus []*skeleton.Menu) *MultiMenu {
	temp := MultiMenu{menus}
	return &temp
}

func (t *MultiMenu) AddMenu(child *skeleton.Menu) {
	t.Menus = append(t.Menus, child)
}

// Data for Template "MessageDiv"
type MessageDiv struct {
	MessageIDCSS string
	ShowMessage  bool
	Message      string
}

func NewMessageDiv(idCSS []string, msg string) *MessageDiv {
	temp := MessageDiv{strings.Join(idCSS, " "), true, msg}
	return &temp
}

func NewNoMessageDiv(idCSS []string) *MessageDiv {
	temp := MessageDiv{strings.Join(idCSS, " "), false, ""}
	return &temp
}

// Data for Template "WebHUD"
type WebHUD struct {
	HeaderData  *skeleton.Header
	Menus       *MultiMenu
	SessionData *MessageDiv
}

func NewWebHUD(head *skeleton.Header, menus *MultiMenu, sessData *MessageDiv) *WebHUD {
	temp := WebHUD{head, menus, sessData}
	return &temp
}

// Data for template "TitleDescriptionText"
type TitleDescriptionText struct {
	EditorCSSID string
	Title       *skeleton.EditableDiv
	Description *skeleton.EditableDiv
	Text        *skeleton.EditableDiv
}

func NewTitleDescriptionText(editorCSSID string, title, description, text *skeleton.EditableDiv) *TitleDescriptionText {
	temp := TitleDescriptionText{editorCSSID, title, description, text}
	return &temp
}
