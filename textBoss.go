package goTem

import (
	"io"
	"text/template"
)

type TextBoss struct {
	manager *templateManager
}

func NewTextBoss() *TextBoss {
	temp := TextBoss{newTemplateManager()}
	return &temp
}

func (h *TextBoss) AddTemplate(mainFile string, dependentTemplates []string) (string, error) {
	return h.manager.AddTemplate(mainFile, dependentTemplates)
}

func (h *TextBoss) ExecuteTemplate(wr io.Writer, mainFileName string, data interface{}) error {
	files, err := h.manager.GetFilenames(mainFileName)
	if err != nil {
		return err
	}
	t, err := template.ParseFiles(files...)
	if err != nil {
		return err
	}
	return t.Execute(wr, data)
}
