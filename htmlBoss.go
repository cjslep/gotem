package goTem

import (
	"html/template"
	"io"
)

type HTMLBoss struct {
	manager *templateManager
}

func NewHTMLBoss() *HTMLBoss {
	temp := HTMLBoss{newTemplateManager()}
	return &temp
}

func (h *HTMLBoss) AddTemplate(mainFile string, dependentTemplates []string) (string, error) {
	return h.manager.AddTemplate(mainFile, dependentTemplates)
}

func (h *HTMLBoss) ExecuteTemplate(wr io.Writer, mainFileName string, data interface{}) error {
	files, err := h.manager.GetFilenames(mainFileName)
	if err != nil {
		return err
	}
	t, err := template.ParseFiles(files...)
	if err != nil {
		return err
	}
	return t.Execute(wr, data)
}
